from datetime import datetime, timedelta
from enum import Enum
from threading import Timer
import logging
import sys


def decimal_to_binary(val):
    """Converts a decimal value to binary
    """
    return int(f"{val:b}")

def set_timeout(fn, ms, *args, **kwargs):
    """ Execute a function after the defined milliseconds

    :param fn: function to execute
    :param ms: number of milliseconds to wait
    """
    time = Timer(ms / 1000., fn, args=args, kwargs=kwargs)
    time.start()
    return time

def get_file_content(filename):
    """Given a filename, return its content

    :param filename: the file path
    """
    lines = []
    with open(filename, 'r') as file:
        lines = file.readlines()
        file.close()
    return lines

class AgeFilter(Enum):
    BELOW = 'below'
    ABOVE = 'above'

def filter_by_age(files, months=1, filter_type: AgeFilter = AgeFilter.BELOW):
    """Return elements filtered by age
    
    files are a tuple of (filename, date)
    """
    filters = []
    for (filename, file_date) in files:
        last_date = datetime.now() - timedelta(days=30 * months)
        if filter_type == AgeFilter.BELOW:
            if file_date > last_date:
                filters.append(filename)
        elif filter_type == AgeFilter.ABOVE:
            if file_date < last_date:
                filters.append(filename)
    return filters

# logging.basicConfig(filename='logs.txt', encoding='utf8', level=logging.DEBUG, format='%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s')
# logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
def get_log(name="anpar"):
    """Returns log object"""
    return logging.getLogger(name)
