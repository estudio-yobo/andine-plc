import PySimpleGUI as sg

COLORS = {
    'PRIMARY': '#2800FF',
    'SECONDARY': '#FF4F03',
    'WHITE': '#FFFFFF',
    'BLACK': '#000000',
    'SUCCESS': '#226600',
    'WARNING': '#7f1919'
}

my_theme = {
    "BACKGROUND": COLORS["WHITE"],
    "TEXT": COLORS["BLACK"],
    "INPUT": COLORS["WHITE"],
    "TEXT_INPUT": COLORS["BLACK"],
    "SCROLL": COLORS["PRIMARY"],
    "BUTTON": (COLORS["WHITE"], COLORS["PRIMARY"]),
    "PROGRESS": sg.DEFAULT_PROGRESS_BAR_COMPUTE,
    "BORDER": 1,
    "SLIDER_DEPTH": 0,
    "PROGRESS_DEPTH": 0,
}

def set_theme():
    """Initialize the theme
    """
    sg.theme_add_new('My Theme', my_theme)

    font = ('Arial', 12, 'bold')
    sg.theme('My Theme')
    sg.set_options(font=font)