""" Control characters
"""

SOH = chr(0x1)
""" Start of heading"""
STX = chr(0x2)
""" Start of text"""
RSX = chr(0x1E)
""" Record separator"""
ETX = chr(0x3)
""" End of text"""
EOT = chr(0x4)
""" End of transmission"""
ACK = chr(0x6)
""" Acknowledge"""
NAK = chr(0x15)
""" Negative Acknowledge"""
