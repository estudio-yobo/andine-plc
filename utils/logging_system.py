import logging
from time import sleep
from utils.control_characters import SOH
from logging.handlers import TimedRotatingFileHandler, RotatingFileHandler

BACKUP_COUNT = 20
MAX_BYTES_SIZE = 15 * 1000 * 1000 # 15MB

class ReadsFilter(logging.Filter):
    def filter(self, record):
        return record.getMessage().startswith(SOH)

class NoReadsFilter(logging.Filter):
    def filter(self, record):
        return not record.getMessage().startswith(SOH)

def event_logs():
    logger = logging.getLogger('com.estudioyobo.anpar-andine')
    logger.setLevel(logging.DEBUG)

    log_format = logging.Formatter('[%(levelname)s] %(asctime)s - %(message)s')

    timed_handler = TimedRotatingFileHandler('reads.log', backupCount=BACKUP_COUNT, when='d', interval=1, encoding='utf8')
    timed_handler.addFilter(ReadsFilter())
    logger.addHandler(timed_handler)

    stream_handler = RotatingFileHandler('events.log', maxBytes=MAX_BYTES_SIZE, backupCount=1, encoding='utf8')
    stream_handler.setFormatter(log_format)
    stream_handler.addFilter(NoReadsFilter())
    logger.addHandler(stream_handler)

    return logger
