# ANPAR

## Setup

- `pip install pipenv` [esto instala la herramienta pipenv, que ayuda a instalar las dependencias de los proyectos]
- `python -m pip install pywin32` [esto igual no hace falta, instala una librería si no está ya, para que se ejecute el código]
- `pipenv install` [crea un entorno donde instala las dependencias del proyecto. Se ejecuta desde la carpeta del proyecto]
- `pipenv shell` [entra en en entorno previamente creado. Es como cargar las dependencias en el entorno]
- `python main.py` [ejecuta el programa]

Para próximas ejecuciones solo haría falta ir al directorio del proyecto y ejecutar las dos últimas instrucciones (entrar en el entorno y ejecutar el programa).

## Ejecutable

En caso de querer generar el EXE, hay una herramienta que te ayuda a ello:

- `python -m pip install psgcompiler` [instala de forma global el PSGCompiler]

Para ejecutarlo es poniendo el comando `psgcompiler` en la consola.
En _Browse_ del python script seleccionas el `main.py` y _Convertir_.
