class Server():
    """ Server config definition
    """
    host: str
    port: int
    local_port: int
    num_ports: int
    delay: int
    country: str
    country_code: str
    delegation_code: str
    classifier: str

    def __init__(self, data: dict):
        self.host = data.get('host')
        self.port = data.get('port')
        self.local_port = data.get('localPort')
        self.num_ports = data.get('numPorts')
        self.delay = data.get('delay')
        self.country = data.get('country')
        self.country_code = data.get('countryCode')
        self.delegation_code = data.get('delegationCode')
        self.id_class = data.get('idClass', 'ANP1')

    def update_host(self, host:str):
        """set the host"""
        self.host = host

    def update_port(self, port:str):
        """set the port"""
        self.port = int(port)

    def update_local_port(self, port:str):
        """set the local port"""
        self.local_port = int(port)

    def update_delay(self, delay:str):
        """set the delay"""
        self.delay = int(delay)

    def update_num_ports(self, num:str):
        """set the number of ports"""
        self.num_ports = int(num)

    def update_country(self, country:str):
        """set the country"""
        self.country = country

    def update_country_code(self, code:str):
        """set the country code"""
        self.country_code = code

    def update_delegation_code(self, code:str):
        """set the delegation code"""
        self.delegation_code = code

    def update_id_class(self, classifier:str):
        """set the delegation classifier"""
        self.id_class = classifier

    def update(self, field: str, value):
        """set the value to the field

        :param field: the field to update
        :param value: the value to set
        """
        do = f"update_{field}"
        if hasattr(self, do) and callable(func := getattr(self, do)):
            func(value)

    def to_dict(self):
        """Server state as a dictionary"""
        return {
            "host": self.host,
            "port": self.port,
            "localPort": self.local_port,
            "numPorts": self.num_ports,
            "delay": self.delay,
            "country": self.country,
            "countryCode": self.country_code,
            "delegationCode": self.delegation_code,
            "idClass": self.id_class,
        }

    def __str__(self):
        return f"{self.host},{self.port},{self.local_port},{self.num_ports},{self.delay},{self.country},{self.country_code},{self.delegation_code},{self.classifier}"
