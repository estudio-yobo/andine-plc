from os import walk, remove, rename

def filelist(path):
    """Returns a list of files in the given path
    """
    files = []

    for (_dirpath, _dirnames, filenames) in walk(path):
        files.extend(filenames)
        break

    return files

class NAS():
    """ NAS config definition
    """
    direction: str
    root_folder: str
    dest_folder: str

    def __init__(self, data: dict):
        self.direction = data['direction']
        self.root_folder = data['rootFolder']
        self.dest_folder = data['destFolder']

    def to_dict(self):
        """NAS state as a dictionary"""
        return {
            "direction": self.direction,
            "rootFolder": self.root_folder,
            "destFolder": self.dest_folder,
        }

    def update_direction(self, direction):
        """Set the direction"""
        self.direction = direction

    def update_root_folder(self, root_folder):
        """Set the root folder"""
        self.root_folder = root_folder

    def update_dest_folder(self, dest_folder):
        """Set the destination folder"""
        self.dest_folder = dest_folder

    def update(self, field: str, value):
        """Set the value to the field
        
        :param field: the field to update
        :param value: the value to set
        """
        do = f"update_{field}"
        if hasattr(self, do) and callable(func := getattr(self, do)):
            func(value)

    def clean_photos(self):
        """Remove files from the root folder
        """
        direction = self.direction + self.root_folder
        files = filelist(direction)
        for f in files:
            remove(f"{direction}/{f}")

    def rename_photo(self, new_name, extension=".jpeg"):
        """Rename the first file in the root directory
        """
        direction = self.direction + self.root_folder
        dest = self.direction + self.dest_folder
        files = filelist(direction)
        if len(files) > 0:
            f = files[0]
            rename(f"{direction}/{f}", f"{dest}/{new_name}{extension}")

