import db
from models.alarm import Alarm
from models.ftp import FTPServer
from models.nas import NAS
from models.plc import PLC
from models.server import Server


class Config():
    """  Config definition
    """
    def __init__(self, server: Server, plc: PLC, nas: NAS, ftp: FTPServer, alarms: list[dict]):
        self.server = server
        self.plc = plc
        self.nas = nas
        self.ftp = ftp
        self.alarms = list(map(Alarm, alarms))

    def update_server(self, field, value):
        """Update the server config"""
        self.server.update(field, value)

    def update_plc(self, field, value):
        """Update the plc config"""
        self.plc.update(field, value)

    def update_nas(self, field, value):
        """Update the nas config"""
        self.nas.update(field, value)

    def update_ftp(self, field, value):
        """Update the ftp config"""
        self.ftp.update(field, value)

    def restore(self):
        """Set config values to db values"""
        cfg = db.get_config()
        self.server = cfg.server
        self.plc = cfg.plc
        self.nas = cfg.nas
        self.ftp = cfg.ftp

    def to_dict(self):
        """Config state as a dictionary"""
        return {
            "server": self.server.to_dict(),
            "plc": self.plc.to_dict(),
            "nas": self.nas.to_dict(),
            "ftp": self.ftp.to_dict(),
            "alarms": [a.to_dict() for a in self.alarms]
        }

    def save(self):
        """Store config state in the database"""
        db.update_config(self)
