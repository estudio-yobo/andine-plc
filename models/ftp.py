from datetime import datetime
from ftplib import FTP, all_errors
from io import BytesIO

class FTPException(BaseException):
    """FTP exception"""
    pass

class FTPServer():
    """ FTP config definition
    """
    host: str
    root_folder: str
    dest_folder: str
    backup_folder: str
    connection = None

    def __init__(self, data: dict):
        self.host = data.get('host')
        self.user = data.get('user')
        self.password = data.get('password')
        self.root_folder = data.get('rootFolder')
        self.dest_folder = data.get('destFolder')
        self.backup_folder = data.get('backupFolder')

    def connect(self):
        """Connect and login to ftp server and
        go to root folder
        """
        try:
            self.connection = FTP(self.host)
            self.connection.login(self.user, self.password)
            self.connection.cwd(self.root_folder)
        except all_errors:
            raise FTPException()
        except Exception:
            raise FTPException()

    def to_dict(self):
        """FTP state as a dictionary"""
        return {
            "host": self.host,
            "user": self.user,
            "password": self.password,
            "rootFolder": self.root_folder,
            "destFolder": self.dest_folder,
            "backupFolder": self.backup_folder,
        }

    def update_host(self, host):
        """Set the host"""
        self.host = host

    def update_user(self, user):
        """Set the user"""
        self.user = user

    def update_password(self, password):
        """Set the password"""
        self.password = password

    def update_root_folder(self, root_folder):
        """Set the root folder"""
        self.root_folder = root_folder

    def update_dest_folder(self, dest_folder):
        """Set the destination folder"""
        self.dest_folder = dest_folder

    def update_backup_folder(self, backup_folder):
        """Set the backup folder"""
        self.backup_folder = backup_folder

    def update(self, field: str, value):
        """Set the value to the field
        
        :param field: the field to update
        :param value: the value to set
        """
        do = f"update_{field}"
        if hasattr(self, do) and callable(func := getattr(self, do)):
            func(value)

    def clean_photos(self):
        """Remove files from the root folder
        """
        files = self.list_files()
        for f in files:
            self.connection.delete(f)

    def list_files(self):
        """list files from the root folder
        """
        if not self.connection:
            self.connect()

        list_files = []
        files = self.connection.mlsd()
        for (f, data) in files:
            if (data['type'] == 'file'):
                list_files.append(f)
        return list_files

    def list_age_files(self, path: str):
        """list files from the root folder
        """
        if not self.connection:
            self.connect()
        try:
            list_files = []
            files = self.connection.mlsd(path)
            for (f, data) in files:
                if (data['type'] == 'file'):
                    parsed_date = datetime.strptime(data['modify'], '%Y%m%d%H%M%S')
                    list_files.append((f, parsed_date))
            return list_files
        except all_errors:
            raise FTPException()

    def remove_file(self, filename: str):
        """Remove file
        """
        if not self.connection:
            self.connect()

        self.connection.delete(filename)

    def get_file(self, filename):
        """get file as binary
        """
        if not self.connection:
            self.connect()
        try:
            flo = BytesIO()
            self.connection.retrbinary(f'RETR {filename}', flo.write)
            flo.seek(0)
            return flo
        except Exception:
            return None

    def backup_file(self, filename, extension=".jpeg"):
        """Copy file to backup folder
        """
        if not self.connection:
            self.connect()

        flo = BytesIO()
        self.connection.retrbinary(f'RETR {self.dest_folder}/{filename}{extension}', flo.write)
        flo.seek(0)
        self.connection.storbinary(f'STOR {self.backup_folder}/{filename}{extension}', flo)

    def rename_photo(self, new_name, extension=".jpeg"):
        """Rename the first file in the root directory
        """
        files = self.list_files()
        if len(files) > 0:
            f = files[0]
            self.connection.rename(f, f"{self.dest_folder}/{new_name}{extension}")
