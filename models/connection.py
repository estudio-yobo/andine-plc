from __future__ import annotations
from typing import TYPE_CHECKING
from models import ConnectionState
from models.modbus import ModbusConnection, Framer, LoggingContextReader, ScraperFactory
from models.plc import PLC
from models.server_connection import ServerConnection, SocketClientFactory

if TYPE_CHECKING:
    from models.session import Session

class ConnectionManager():
    """Base connection manager class
    """
    state = ConnectionState.INIT

    def __init__(self, session: Session):
        self.session = session

    def update_state(self, state: ConnectionState):
        """Updates the state of the connection
        """
        self.state = state
        self.session.update_connections_state()

class PLCConnectionManager(ConnectionManager):
    """Initializes the PLC connection and handles the connection state
    """
    plc = PLC

    def __init__(self, session: Session):
        super().__init__(session)
        self.plc = session.config.plc
        framer = Framer()
        reader = LoggingContextReader(session)
        factory = ScraperFactory(framer, reader, self.plc, self.update_state)
        
        self.connection = ModbusConnection()
        self.connection.connect(framer, factory, self.plc)     

        self.connection.start()

    def finalize(self):
        """Close the connection"""
        self.connection.close()

class ServerConnectionManager(ConnectionManager):
    """Initializes the Server connection and handles the connection state
    """

    def __init__(self, session: Session):
        super().__init__(session)
        self.server = session.config.server

        client = SocketClientFactory(session, self.update_state)

        self.connection = ServerConnection()
        self.connection.connect(client, self.server)

        self.connection.start()

    def finalize(self):
        """Close the connection"""
        self.connection.close()
