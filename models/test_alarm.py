#pylint: disable=missing-class-docstring|missing-function-docstring
import unittest
from .alarm import Alarm, AlarmState, has_active_alarms

class TestAlarm(unittest.TestCase):
    def test_alarm_is_not_ok(self):
        alarm_data = {
            "title": "Test",
            "alarms": []
        }
        alarm = Alarm(alarm_data)
        self.assertFalse(alarm.alarm_is_ok())

    def test_alarm_is_not_ok_2(self):
        alarm_data = {
            "title": "Test",
            "alarms": [
                {
                    "title": "Test_1",
                    "alarms": []
                }
            ]
        }
        alarm = Alarm(alarm_data)
        self.assertFalse(alarm.alarm_is_ok())

    def test_alarm_is_not_ok_3(self):
        alarm_data = {
            "title": "Test",
            "alarms": [
                {
                    "title": "Test_1",
                    "alarms": []
                }
            ]
        }
        alarm = Alarm(alarm_data)
        alarm.set_state(AlarmState.OK)
        self.assertFalse(alarm.alarm_is_ok())

    def test_alarm_is_not_ok_4(self):
        alarm_data = {
            "title": "Test",
            "alarms": [
                {
                    "title": "Test_1",
                    "alarms": []
                },
                {
                    "title": "Test_2",
                    "alarms": []
                }
            ]
        }
        alarm = Alarm(alarm_data)
        alarm.set_state(AlarmState.OK)
        alarm.alarms[0].set_state(AlarmState.OK)
        self.assertFalse(alarm.alarm_is_ok())

    def test_alarm_is_not_ok_5(self):
        alarm_data = {
            "title": "Test",
            "alarms": [
                {
                    "title": "Test_1",
                    "alarms": []
                },
                {
                    "title": "Test_2",
                    "alarms": []
                }
            ]
        }
        alarm = Alarm(alarm_data)
        alarm.set_state(AlarmState.OK)
        alarm.alarms[1].set_state(AlarmState.OK)
        self.assertFalse(alarm.alarm_is_ok())

    def test_alarm_is_not_ok_6(self):
        alarm_data = {
            "title": "Test",
            "alarms": [
                {
                    "title": "Test_1",
                    "alarms": []
                },
                {
                    "title": "Test_2",
                    "alarms": []
                }
            ]
        }
        alarm = Alarm(alarm_data)
        alarm.set_state(AlarmState.KO)
        alarm.alarms[0].set_state(AlarmState.OK)
        alarm.alarms[1].set_state(AlarmState.OK)
        self.assertFalse(alarm.alarm_is_ok())

    def test_alarm_is_ok(self):
        alarm_data = {
            "title": "Test",
            "alarms": []
        }
        alarm = Alarm(alarm_data)
        alarm.set_state(AlarmState.OK)
        self.assertTrue(alarm.alarm_is_ok())

    def test_alarm_is_ok_reserva(self):
        alarm_data = {
            "title": "Test",
            "alarms": [
                {
                    "title": "_Reserva",
                    "alarms": []
                }
            ]
        }
        alarm = Alarm(alarm_data)
        alarm.set_state(AlarmState.OK)
        self.assertTrue(alarm.alarm_is_ok())

    def test_alarm_is_ok_reserva_2(self):
        alarm_data = {
            "title": "Test",
            "alarms": [
                {
                    "title": "_Reserva",
                    "alarms": []
                },
                {
                    "title": "Testing",
                    "alarms": []
                }
            ]
        }
        alarm = Alarm(alarm_data)
        alarm.set_state(AlarmState.OK)
        alarm.alarms[1].set_state(AlarmState.OK)
        self.assertTrue(alarm.alarm_is_ok())

    def test_alarm_is_ok_2(self):
        alarm_data = {
            "title": "Test",
            "alarms": [
                {
                    "title": "Test_1",
                    "alarms": []
                }
            ]
        }
        alarm = Alarm(alarm_data)
        alarm.set_state(AlarmState.OK)
        alarm.alarms[0].set_state(AlarmState.OK)
        self.assertEqual(alarm.alarms[0].title, "Test_1")
        self.assertTrue(alarm.alarm_is_ok())

    def test_alarm_is_ok_3(self):
        alarm_data = {
            "title": "Test",
            "alarms": [
                {
                    "title": "Test_1",
                    "alarms": []
                },
                {
                    "title": "Test_2",
                    "alarms": []
                }
            ]
        }
        alarm = Alarm(alarm_data)
        alarm.set_state(AlarmState.OK)
        alarm.alarms[0].set_state(AlarmState.OK)
        self.assertEqual(alarm.alarms[0].title, "Test_1")
        alarm.alarms[1].set_state(AlarmState.OK)
        self.assertEqual(alarm.alarms[1].title, "Test_2")
        self.assertTrue(alarm.alarm_is_ok())

    def test_has_active_alarms(self):
        alarm_data = {
            "title": "Test",
            "alarms": [
                {
                    "title": "Test_1",
                    "alarms": []
                },
                {
                    "title": "Test_2",
                    "alarms": []
                }
            ]
        }
        alarm = Alarm(alarm_data)
        alarm.set_state(AlarmState.OK)
        alarm.alarms[0].set_state(AlarmState.OK)
        alarm.alarms[1].set_state(AlarmState.OK)

        alarm2_data = {
            "title": "Test",
            "alarms": []
        }
        alarm2 = Alarm(alarm2_data)
        alarm2.set_state(AlarmState.KO)

        self.assertTrue(has_active_alarms([alarm,alarm2]))

    def test_has_not_active_alarms(self):
        alarm_data = {
            "title": "Test",
            "alarms": [
                {
                    "title": "Test_1",
                    "alarms": []
                },
                {
                    "title": "Test_2",
                    "alarms": []
                }
            ]
        }
        alarm = Alarm(alarm_data)
        alarm.set_state(AlarmState.OK)
        alarm.alarms[0].set_state(AlarmState.OK)
        alarm.alarms[1].set_state(AlarmState.OK)

        alarm2_data = {
            "title": "Test",
            "alarms": []
        }
        alarm2 = Alarm(alarm2_data)
        alarm2.set_state(AlarmState.OK)

        self.assertFalse(has_active_alarms([alarm,alarm2]))
    
    def test_full_alarms(self):
        a1_data = {
            "title": "Comunicaci\u00f3n con Servidor de datos",
            "alarms": []
        }
        a2_data = {
            "title": "Comunicaci\u00f3n con PLC",
            "alarms": [
                {
                "title": "_Reserva",
                "alarms": []
                },
                {
                "title": "Pesaje",
                "alarms": []
                },
                {
                "title": "Volum\u00e9trico",
                "alarms": []
                },
                {
                "title": "C\u00e1mara de fotos",
                "alarms": []
                }
            ]
        }
        a1 = Alarm(a1_data)
        a2 = Alarm(a2_data)
        alarms = [a1, a2]

        self.assertTrue(has_active_alarms(alarms))

    def test_full_alarms_2(self):
        a1_data = {
            "title": "Comunicaci\u00f3n con Servidor de datos",
            "alarms": []
        }
        a2_data = {
            "title": "Comunicaci\u00f3n con PLC",
            "alarms": [
                {
                "title": "_Reserva",
                "alarms": []
                },
                {
                "title": "Pesaje",
                "alarms": []
                },
                {
                "title": "Volum\u00e9trico",
                "alarms": []
                },
                {
                "title": "C\u00e1mara de fotos",
                "alarms": []
                }
            ]
        }
        a1 = Alarm(a1_data)
        a1.set_state(AlarmState.OK)
        a2 = Alarm(a2_data)
        a2.set_state(AlarmState.OK)
        a2.alarms[1].set_state(AlarmState.OK)
        a2.alarms[2].set_state(AlarmState.OK)
        a2.alarms[3].set_state(AlarmState.OK)
        alarms = [a1, a2]

        self.assertFalse(has_active_alarms(alarms))

if __name__ == '__main__':
    unittest.main()