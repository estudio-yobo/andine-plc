from __future__ import annotations
from typing import TYPE_CHECKING
from threading import Thread
from twisted.internet import reactor
from twisted.internet.protocol import ReconnectingClientFactory
from twisted.protocols.basic import LineReceiver
from twisted.internet.error import ReactorAlreadyRunning
from models import ConnectionState
from utils.logging_system import event_logs
from utils.control_characters import ACK, EOT, ETX, NAK, RSX, SOH, STX

if TYPE_CHECKING:
    from models.server import Server
    from models.session import Session

log = event_logs()

# --------------------------------------------------------------------------- # 
# Define some constants
# --------------------------------------------------------------------------- # 
DELAY = 0.5    # The delay between subsequent reads

class SocketClientProtocol(LineReceiver):
    """A protocol that receives lines and/or raw data, depending on mode.

    In line mode, each line that's received becomes a callback to
    L{lineReceived}.  In raw data mode, each chunk of raw data becomes a
    callback to L{LineReceiver.rawDataReceived}.
    The L{setLineMode} and L{setRawMode} methods switch between the two modes.
    """

    factory: SocketClientFactory

    def lineReceived(self, line):
        log.debug("lineReceived %s", line)
        if line == ACK:
            log.debug("Received ACK")
            self.factory.on_ack()
        else:
            log.debug("Received NAK")
            self.factory.on_nak()

    def connectionMade(self):
        log.debug("connectionMade")
        self.factory.identify_client(self)

    def rawDataReceived(self, data):
        log.debug("rawDataReceived %s", data)
        if ACK in data.decode('ascii'):
            log.debug("Received raw ACK")
            self.factory.on_ack()
        elif NAK in data.decode('ascii'):
            log.debug("Received raw NAK")
            self.factory.on_nak()

class SocketClientFactory(ReconnectingClientFactory):
    """ Created with callbacks for connection and receiving.
        send_msg can be used to send messages when connected.
    """
    state = ConnectionState.INIT
    def __init__(
            self,
            session: Session,
            update_state):
        self.update_state = update_state
        self.client = None
        self.session = session

    def set_state(self, state: ConnectionState):
        """Set connection state and notify parent class
        """
        self.state = state
        self.update_state(self.state)

    def startedConnecting(self, connector):
        self.set_state(ConnectionState.CONNECTING)

    def buildProtocol(self, addr):
        log.debug("Building protocol")
        self.resetDelay()
        protocol = SocketClientProtocol()
        protocol.setRawMode()
        protocol.factory = self
        return protocol

    def clientConnectionFailed(self, connector, reason):
        log.debug("failed")
        self.set_state(ConnectionState.SOCKET_CLOSED)
        ReconnectingClientFactory.clientConnectionFailed(self, connector,
                                                         reason)

    def clientConnectionLost(self, connector, reason):
        log.debug("connection lost")
        self.set_state(ConnectionState.CONNECTING)
        connector.connect()

    def on_ack(self):
        """Called when the response is an ACK
        """
        if self.state == ConnectionState.IDENTIFYING:
            log.debug("ACK Identifying")
            self.set_state(ConnectionState.CONNECTED)
            reactor.callLater(DELAY, self.send_item)
        elif self.state == ConnectionState.CONNECTED:
            log.debug("ACK Connected")
            self.set_state(ConnectionState.SENDING_DATA)
            self.send_item()
        else:
            log.debug("ACK else %s", self.state)
            self.set_state(ConnectionState.CONNECTED)
            reactor.callLater(DELAY, self.send_item)


    def on_nak(self):
        """Called when the response is a NAK
        """
        if self.state == ConnectionState.IDENTIFYING:
            log.error("IDENTIFICACION FALLIDA")
        else:
            log.error("NAK set connected")
            self.set_state(ConnectionState.CONNECTED)
            reactor.callLater(DELAY, self.send_item)

    def identify_client(self, client):
        """Stores the client reference and begins identification
        """
        self.client = client
        # self.set_state(ConnectionState.IDENTIFY)
        self.set_state(ConnectionState.IDENTIFYING)
        self.client.sendLine(self._identification_string())

    def send_item(self):
        """Sends a new item to the server
        """
        item = self.session.get_last_read()
        if self.client and item:
            log.debug("send item %s", item)
            self.client.sendLine(item.to_binary())
        elif self.client:
            log.debug("send dummy")
            self.client.sendLine(self._dummy_string())
        else:
            self.set_state(ConnectionState.SOCKET_CLOSED)
        
        self.set_state(ConnectionState.CONNECTED)

    def _identification_string(self):
        srv = self.session.config.server
        data = [
            f"{SOH}ttIdentificacion;1;tcASCII",
            f"{STX}IDPAIS;IDDELEGACION;IDCLASIFICADOR",
            f"{RSX}{srv.country_code};{srv.delegation_code};{srv.id_class}_{srv.country}{srv.delegation_code}{ETX}",
            f"{EOT}"
        ]
        return "".join(data).encode('ascii')

    def _dummy_string(self):
        return f"{SOH}itDummy;0;tcASCII{EOT}".encode('ascii')

class ServerConnection(Thread):

    def __init__(self):
        Thread.__init__(self)

    def connect(self, factory, server: Server):
        reactor.connectTCP(server.host, server.port, factory)

    def run(self):
        try:
            reactor.run(installSignalHandlers=False)
        except ReactorAlreadyRunning:
            log.debug("Reactor already running exception :(")
            # Ignore error about reactor already running
            pass

    def close(self):
        """Close the reactor"""
        if reactor.running:
            # reactor.stop()
            pass
