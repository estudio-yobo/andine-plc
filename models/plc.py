class PLC():
    """PLC config definition
    """
    host: str
    port: int

    def __init__(self, data: dict):
        self.host = data['host']
        self.port = data['port']
        self.num_reg = data['nReg']
        self.reg = data['reg']
        self.num_reg_w = data['nRegW']
        self.reg_w = data['regW']

    def update_host(self, host:str):
        """Set the host"""
        self.host = host

    def update_port(self, port:str):
        """Set the port"""
        self.port = int(port)

    def update_num_reg(self, num_reg:str):
        """Set the number of registers"""
        self.num_reg = int(num_reg)

    def update_reg(self, reg:str):
        """Set the first register"""
        self.reg = int(reg)

    def update_num_reg_w(self, num_reg_w:str):
        """Set the number of write registers"""
        self.num_reg_w = int(num_reg_w)

    def update_reg_w(self, reg_w:str):
        """Set the first write register"""
        self.reg_w = int(reg_w)

    def update(self, field: str, value):
        """Set the value to the field
        
        :param field: the field to update
        :param value: the value to set
        """
        do = f"update_{field}"
        if hasattr(self, do) and callable(func := getattr(self, do)):
            func(value)

    def to_dict(self):
        """PLC state as a dictionary"""
        return {
            "host": self.host,
            "port": self.port,
            "reg": self.reg,
            "regW": self.reg_w,
            "nReg": self.num_reg,
            "nRegW": self.num_reg_w,
        }

    def __str__(self):
        return f"{self.host},{self.port}"
