#pylint: disable=missing-class-docstring|missing-function-docstring
import unittest
from datetime import datetime
from .item import FULL_DATE_FORMAT, Item, SOH, STX, RSX, EOT, ETX

class TestItem(unittest.TestCase):
    def test_factory(self):
        barcode = "0125405466"
        depth = 10
        height = 20
        width = 30
        weight = 75
        date = "20220202111912"
        data = [
            f"{SOH}ttClasificacion;1;tcASCII",
            f"{STX}CLDCODBARRAS;CLDALTO;CLDANCHO;CLDLARGO;CLDPESO;CLDINDUCCION;CLDFECINDUCCION;CLDRAMPA;CLDFECCLASIFICACION;",
            f"{RSX}{barcode};{height};{depth};{width};{weight};IU012;{date};0012;{date};ABCDEFGHIJKLMNOPQRSTUVWXYZ{ETX}",
            f"{EOT}",
        ]
        item_str = "".join(data)
        item = Item.factory(item_str)

        self.assertEqual(item.barcode, barcode)
        self.assertEqual(item.weight, weight)
        self.assertEqual(item.size.depth, depth)
        self.assertEqual(item.size.height, height)
        self.assertEqual(item.size.width, width)
        self.assertEqual(item.date,  datetime.strptime(date, FULL_DATE_FORMAT))

if __name__ == '__main__':
    unittest.main()
