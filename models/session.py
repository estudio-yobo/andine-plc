import PySimpleGUI as sg
from components.alarm import update_list
from components.image_preview import get_img_data
import db
from models.alarm import AlarmState
from models.config import Config
from models.item import Item
from models.ftp import FTPException
from models.connection import PLCConnectionManager, ServerConnectionManager
from models import ConnectionState
from utils import AgeFilter, decimal_to_binary, filter_by_age, set_timeout
from utils.logging_system import event_logs

log = event_logs()

class Session():
    """ Manages the app session data
    """
    reads = 0
    config: Config
    window: sg.Window = None
    server_connection: ServerConnectionManager = None
    plc_connection: PLCConnectionManager = None
    readings: list[Item] = []

    def __init__(self):
        self.config = db.get_config()
        self.remove_age_files()

    def new_read(self, item: Item):
        """ Increases the number of reads, adds the item to log and renames the item photo
        """
        self.reads += 1
        log.info(item.definition())
        self.readings.append(item)
        filename, extension = item.get_filename()
        self.config.ftp.rename_photo(filename, extension=extension)
        self.config.ftp.backup_file(filename, extension=extension)
        if self.window:
            self.window['-READS-'].update(self.reads)
        self.__data_received(item)

    def get_last_read(self):
        """ returns the last read and removes it from the readings list
        """
        if len(self.readings) > 0:
            item = self.readings.pop()
            return item
        return None

    def set_window(self, window: sg.Window):
        """adds the window reference to the session
        """
        self.window = window

    def start_connections(self):
        """ Initializes the connection to server and PLC
        """
        self.plc_connection = PLCConnectionManager(self)
        self.server_connection = ServerConnectionManager(self)

    def finalize(self):
        self.plc_connection.finalize()
        self.server_connection.finalize()

    def __data_received(self, item: Item):
        if self.window:
            self.window['-BARCODE-'].update(item.barcode)
            self.window['-SIZE_HEIGHT-'].update(item.size.height)
            self.window['-SIZE_WIDTH-'].update(item.size.width)
            self.window['-SIZE_DEPTH-'].update(item.size.depth)
            self.window['-WEIGHT-'].update(item.weight)
            self.window['-DATE-'].update(item.get_date('%Y-%m-%d %H:%M:%S'))
        set_timeout(self.__show_image, 200, item)

    def __show_image(self, item: Item):
        if self.window:
            filename, extension = item.get_filename()
            # data = get_img_data(f"{self.config.nas.direction}/{self.config.nas.dest_folder}/{filename}{extension}", maxsize=(330, 330))
            img_file = self.config.ftp.get_file(f"{self.config.ftp.dest_folder}/{filename}{extension}")
            if img_file:
                data = get_img_data(img_file, maxsize=(550, 550))
                if data:
                    self.window['-PREVIEW_IMAGE-'].update(data=data)

    def update_connections_state(self):
        """ updates the alarms UI
        """
        for i, alarm in enumerate(self.config.alarms):
            if "PLC" in alarm.title and self.plc_connection:
                self.config.alarms[i].set_state(AlarmState.OK if self.plc_connection.state >= ConnectionState.CONNECTED else AlarmState.KO)
            elif "Servidor" in alarm.title and self.server_connection:
                self.config.alarms[-2].set_state(AlarmState.OK if self.server_connection.state >= ConnectionState.CONNECTED else AlarmState.KO)

        update_list(self.window, self.config.alarms)

    def set_alarms_state(self, value: int):
        """changes the PLC alarm state based on the value binary representation and upates the UI
        """
        to_binary = decimal_to_binary(value)
        alarm_state = str(to_binary).zfill(15)[::-1] # reverse 15 digit string
        alarm_state_list = list(alarm_state)
        
        for i, alarm in enumerate(self.config.alarms):
            if "PLC" in alarm.title:
                for j in range(0, len(alarm.alarms)):
                    self.config.alarms[i].alarms[j].set_state(AlarmState.OK if alarm_state_list[j] == "0" else AlarmState.KO)

        self.update_connections_state()

    def save_server_data(self, *args, **kwargs):
        """Updates the server direction in UI
        """
        if self.window:
            self.window['-SERVER-'].update(self.config.server.host, **kwargs)

    def remove_age_files(self):
        """remove backup files which are more than 6 months old"""
        try:
            files = self.config.ftp.list_age_files(self.config.ftp.backup_folder)
            filtered = filter_by_age(files, months=6, filter_type=AgeFilter.ABOVE)
            for f in filtered:
                self.config.ftp.remove_file(f"{self.config.ftp.backup_folder}/{f}")
        except FTPException:
            log.debug('Error in FTP')