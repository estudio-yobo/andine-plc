import re
from datetime import datetime
import locale

from utils.control_characters import SOH, STX, RSX, ETX, EOT
from .size import Size

locale.setlocale(locale.LC_ALL, 'es_ES.UTF-8')

FULL_DATE_FORMAT = '%Y%m%d%H%M%S'
DATE_FORMAT = '%y%m%d%H%M%S'

def parse_item_from_response(response):
    """  Creates an Item from a PLC Response

    weight is received in kgs

    :param response: The response to process
    :return: An Item
    """
    barcode = ""
    for i in range(2, 36):
        barcode += chr(response.getRegister(i))

    height = response.getRegister(36)
    width = response.getRegister(37)
    depth = response.getRegister(38)
    weight = response.getRegister(39)
    size = Size(width, height, depth)
    return Item(barcode, size, weight)

class Item():
    """ The item read from the PLC

    weight is Kgs

    """
    def __init__(self, barcode: str, size: Size, weight: float, date: datetime = None):
        self.barcode = barcode
        self.size = size
        self.weight = weight
        d = datetime.today() if date is None else date
        self.date = d

    def get_date(self, fmt = DATE_FORMAT):
        """returns the date formatted"""
        return self.date.strftime(fmt)

    def get_filename(self):
        """returns the image file name"""
        return (f"IMG_ANP_{self.get_date()}", ".jpeg")

    def definition(self):
        """Returns the definition string for the item

        weight has to be sent as grams

        """
        weight_in_grams = self.weight * 1000
        data = [
            f"{SOH}ttClasificacion;1;tcASCII",
            f"{STX}CLDCODBARRAS;CLDALTO;CLDANCHO;CLDLARGO;CLDPESO;CLDINDUCCION;CLDFECINDUCCION;CLDRAMPA;CLDFECCLASIFICACION;CLDALIBICODE",
            f"{RSX}{self.barcode};{locale.format_string('%.2f',self.size.height)};{locale.format_string('%.2f',self.size.depth)};{locale.format_string('%.2f',self.size.width)};{locale.format_string('%.2f',weight_in_grams)};IU001;{self.get_date(FULL_DATE_FORMAT)};0001;{self.get_date(FULL_DATE_FORMAT)};{self.get_date(DATE_FORMAT)}{ETX}"
            f"{EOT}"
        ]
        return "".join(data)

    def to_binary(self):
        """Returns the data as a binary string"""
        data = self.definition()
        return data.encode('ascii')

    def __str__(self):
        return f"[{self.get_date()}] {self.barcode} {self.size} {self.weight}"

    def factory(parse: str):
        """ Creates an Item from a string

        weight is received in grams

        :param parse: string to parse
        """
        m = re.search(f'{RSX}(.*){ETX}', parse)
        data = m.group(1)
        barcode, height, depth, width, weight, _ind, date, _ramp, _other_date, _abc = data.split(';')

        # Parse floats with commas as decimal divider instead of dots
        safe_width = locale.atof(width)
        safe_height = locale.atof(height)
        safe_depth = locale.atof(depth)
        safe_weight = locale.atof(weight) / 1000

        item_size = Size(safe_width, safe_height, safe_depth)
        parsed_date = datetime.strptime(date, FULL_DATE_FORMAT)
        return Item(barcode, item_size, safe_weight, date=parsed_date)
    factory = staticmethod(factory)
