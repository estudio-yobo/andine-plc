class Size():
    """Object containing item sizes"""
    def __init__(self, width: float, height: float, depth: float):
        self.width = width
        self.height = height
        self.depth = depth

    def __str__(self):
        return f"{self.width}x{self.height}x{self.depth}"

    def factory(content: str):
        """Creates a Size object from a string"""
        width, height, depth = content.split('x')
        return Size(width, height, depth)
    factory = staticmethod(factory)
