"""
This is a simple scraper that can be pointed at a
modbus device to pull down all its values and store
them as a collection of sequential data blocks.
"""
from __future__ import annotations
from typing import TYPE_CHECKING
from threading import Thread
from twisted.internet import serialport, reactor
from twisted.internet.protocol import ReconnectingClientFactory
from twisted.internet.error import ReactorAlreadyRunning
from pymodbus.factory import ClientDecoder
from pymodbus.client.asynchronous.twisted import ModbusClientProtocol
from pymodbus.transaction import ModbusSocketFramer
from models import ConnectionState
from models.item import parse_item_from_response, Item
from models.plc import PLC
from utils import decimal_to_binary, set_timeout
from utils.logging_system import event_logs
if TYPE_CHECKING:
    from models.session import Session

log = event_logs()

# --------------------------------------------------------------------------- # 
# Define some constants
# --------------------------------------------------------------------------- # 
DELAY = 0.1    # The delay between subsequent reads
SLAVE = 0x01  # The slave unit id to read from

class EndpointInterface():
    """Interface to follow from ScraperProtocol"""
    def write(self, response: any):
        pass

    def clean(self):
        pass

# --------------------------------------------------------------------------- # 
# A simple scraper protocol
# --------------------------------------------------------------------------- # 
# I tried to spread the load across the device, but feel free to modify the
# logic to suit your own purpose.
# --------------------------------------------------------------------------- #
class ScraperProtocol(ModbusClientProtocol):
    """A simple scraper protocol"""

    address = None
    previous = None
    initialized = False
    puntero = 0

    def __init__(self, framer, endpoint: EndpointInterface):
        """ Initializes our custom protocol

        :param framer: The decoder to use to process messages
        :param endpoint: The endpoint to send results to
        """
        log.debug(f"Scrape {endpoint}")
        ModbusClientProtocol.__init__(self, framer)
        self.endpoint = endpoint

    def connectionMade(self):
        """ Callback for when the client has connected
        to the remote server.
        """
        super(ScraperProtocol, self).connectionMade()
        log.debug("Beginning the processing loop")
        reactor.callLater(DELAY, self.scrape_holding_registers)
        # reactor.callLater(0.1, self.write_puntero)
        reactor.callLater(DELAY, self.write_holding_registers)

        set_timeout(self.init_variable, 5000)

    def connectionLost(self, reason=None):
        """ Callback for when the client disconnects from the
        server.

        :param reason: The reason for the disconnection
        """
        log.debug("Connection lost")
        reactor.callLater(DELAY, reactor.stop)

    def init_variable(self):
        self.initialized = True

    def scrape_holding_registers(self):
        """ Defer fetching holding registers
        """
        log.debug(f"reading holding registers: {self.factory.plc.reg_w}")
        d = self.read_holding_registers(self.factory.plc.reg_w, count=self.factory.plc.num_reg_w, unit=SLAVE)
        # d.addCallbacks(self.scrape_discrete_inputs, self.error_handler)
        d.addCallbacks(self.start_next_cycle, self.error_handler)

    def write_holding_registers(self):
        """ Write holding register
        """
        log.debug("writing holding registers: %d" % self.factory.plc.reg)
        values = [0 for i in range(1, self.factory.plc.num_reg)]
        # values[0] = 1 # Marcha/paro
        values[1] = self.puntero # Puntero
        values[2] = 1 # destino
        d = self.write_registers(self.factory.plc.reg, values)
        d.addCallbacks(self.start_next_write_cycle, self.error_handler)
    
    def write_puntero(self):
        """ Write puntero
        """
        # log.debug("writing holding registers: %d" % self.factory.plc.reg)
        # values = [0 for i in range(1, self.factory.plc.num_reg)]
        # # values[0] = 1 # Marcha/paro
        # values[1] = 1 # Puntero
        # values[2] = 1 # destino
        d = self.write_register(self.factory.plc.reg_w + 40, 1)
        d = self.write_register(self.factory.plc.reg_w + 41, 14)
        # d.addCallbacks(self.start_next_write_cycle, self.error_handler)

    def start_next_cycle(self, response):
        """ Write values of coils, trigger next cycle

        :param response: The response to process
        """
        clean = decimal_to_binary(response.getRegister(1))

        if clean:
            # clean photos
            self.endpoint.clean()
       
        item = parse_item_from_response(response)
        puntero = response.getRegister(40)
        alarms = response.getRegister(41)

        # for i in range(1, 45):
        #     log.debug("%d: %s" % (i, response.getRegister(i)))
        if not self.initialized and item.barcode:
            self.previous = item.barcode
        elif puntero == 1 and item.barcode and item.barcode != self.previous:
            self.puntero += 1
            log.debug("nuevo")
            if self.puntero > 32000:
                self.puntero = 1
            self.endpoint.write((item, alarms))
            self.previous = item.barcode
        else:
            self.endpoint.write((None, alarms))

        log.debug("starting next round")
        reactor.callLater(DELAY, self.scrape_holding_registers)

    def start_next_write_cycle(self, response):
        log.debug("starting next write")
        reactor.callLater(DELAY, self.write_holding_registers)

    def error_handler(self, failure):
        """ Handle any twisted errors

        :param failure: The error to handle
        """
        log.error(failure)


class ScraperFactory(ReconnectingClientFactory):
    """ This is used to build client protocol's if you tie into twisted's method
    of processing. It basically produces client instances of the underlying
    protocol::
        Factory(Protocol) -> ProtocolInstance

    It also persists data between client instances (think protocol singelton).
    """

    protocol = ScraperProtocol

    def __init__(self, framer, endpoint: EndpointInterface, plc: PLC, update_state):
        """ Remember things necessary for building a protocols """

        self.framer   = framer
        self.endpoint = endpoint
        self.update_state = update_state
        self.plc = plc

    def buildProtocol(self, _):
        """ Create a protocol and start the reading cycle """
        log.debug("buildProtocol")
        self.update_state(ConnectionState.CONNECTED)
        protocol = self.protocol(self.framer, self.endpoint)
        protocol.factory = self
        return protocol

    def startedConnecting(self, connector):
        log.debug("startConecting")
        self.update_state(ConnectionState.CONNECTING)
        return super().startedConnecting(connector)

    def clientConnectionFailed(self, connector, reason):
        log.debug("client connection Failed %s", reason)
        self.update_state(ConnectionState.SOCKET_CLOSED)
        return super().clientConnectionFailed(connector, reason)

    def clientConnectionLost(self, connector, reason):
        log.debug("client connection Lost %s", reason)
        self.update_state(ConnectionState.SOCKET_CLOSED)
        connector.connect()
        return super().clientConnectionLost(connector, reason)


class SerialModbusClient(serialport.SerialPort):
    """A custom client for our device
    """

    def __init__(self, factory, *args, **kwargs):
        """ Setup the client and start listening on the serial port

        :param factory: The factory to build clients with
        """
        protocol = factory.buildProtocol(None)
        self.decoder = ClientDecoder()
        serialport.SerialPort.__init__(self, protocol, *args, **kwargs)



class LoggingContextReader(EndpointInterface):
    """A custom line reader"""

    def __init__(self, session: Session):
        """ Initialize a new instance of the logger

        :param output: The output file to save to
        """
        self.session = session

    def write(self, response: tuple[Item, int]):
        """ Handle the next modbus response

        :param response: The response to process
        """
        item, alarms = response
        if item:
            self.session.new_read(item)
        self.session.set_alarms_state(alarms)
        # self.context.setValues(fx, address, values)

    def clean(self):
        self.session.config.ftp.clean_photos()


def Framer():
    """Initiealizes a new instace of the framer"""
    return ModbusSocketFramer(ClientDecoder())

class ModbusConnection(Thread):

    def __init__(self):
        Thread.__init__(self)

    def connect(self, framer, factory, plc: PLC):
        # how to connect based on TCP vs Serial clients
        if isinstance(framer, ModbusSocketFramer):
            reactor.connectTCP(plc.host, plc.port, factory)
        else:
            SerialModbusClient(factory, plc.port, reactor)

    def run(self):
        try:
            reactor.run(installSignalHandlers=False)
        except ReactorAlreadyRunning:
            pass

    def close(self):
        """Close the reactor"""
        if reactor.running:
            reactor.stop()
