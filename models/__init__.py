from enum import Enum
from functools import total_ordering

from utils.theme import COLORS

@total_ordering
class ConnectionState(Enum):
    INIT = 0
    SOCKET_CLOSED = 1
    PING = 2
    CONNECT_SOCKET = 3
    CONNECTING = 4
    IDENTIFY = 5
    IDENTIFYING = 6
    CONNECTED = 7
    SENDING_DATA = 8
    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self.value < other.value
        return NotImplemented

CONNECTION_STATE_COLORS = {
    ConnectionState.INIT: COLORS['PRIMARY'],
    ConnectionState.SOCKET_CLOSED: COLORS['WARNING'],
    ConnectionState.PING: COLORS['SECONDARY'],
    ConnectionState.CONNECT_SOCKET: COLORS['BLACK'],
    ConnectionState.CONNECTING: COLORS['BLACK'],
    ConnectionState.IDENTIFY: COLORS['BLACK'],
    ConnectionState.IDENTIFYING: COLORS['BLACK'],
    ConnectionState.CONNECTED: COLORS['SUCCESS'],
    ConnectionState.SENDING_DATA: COLORS['BLACK'],
}