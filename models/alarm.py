from enum import Enum

class AlarmState(Enum):
    """Alarm states enum"""
    OK = 'OK'
    KO = 'ERROR'
    WARN = 'WARN'

class Alarm():
    """Object containing alarm data
    """
    title: str
    alarms: list
    state = AlarmState.KO

    def __init__(self, alarm: dict):
        self.title = alarm['title']
        self.alarms = list(map(Alarm, alarm['alarms']))

    def set_state(self, state: AlarmState):
        """Set alarm state"""
        self.state = state

    def get_valid_alarms(self):
        """Get the alarms that does not begin with an underscore

        This is because there are some alarm slots in the config file not used
        """
        return list(filter(lambda a: not a.title.startswith('_'),self.alarms))

    def alarm_is_ok(self, a = None):
        """Check if alarm or nested alarms is ok"""
        alarm = a if a else self
        if len(alarm.alarms) == 0:
            return alarm.title == '_Reserva' or alarm.state == AlarmState.OK
        else:
            return alarm.title == '_Reserva' or alarm.state == AlarmState.OK and [self.alarm_is_ok(a) for a in alarm.alarms].count(True) == len(alarm.alarms)

    def to_dict(self):
        """Alarm state as a dictionary"""
        return {
            "title": self.title,
            "alarms": [a.to_dict() for a in self.alarms]
        }

    def __str__(self):
        return f"{self.title} {self.state} - {self.alarms}"

def has_active_alarms(alarms: list[Alarm]):
    """Checks for alarms with errors

    :return: True if there is at least one alarm with errors or False otherwise
    """
    list_alarm_states = [a.alarm_is_ok() for a in alarms]
    has_alarms = list_alarm_states.count(False) > 0
    return has_alarms