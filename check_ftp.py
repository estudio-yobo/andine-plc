import json
import sys

from models.ftp import FTPServer
from utils import filter_by_age, AgeFilter

CONFIG_FILE = 'db/config.json'
COMMANDS = "Valid commands: [list, clean, rename, backup, age <months?>, remove <months?>]"

def get_config():
    """Parses the configuration file

    :return: Configuration object
    """
    file = open(CONFIG_FILE, 'r')
    try:
        data = json.load(file)
        server = FTPServer(data['ftp'])

    finally:
        file.close()

    return server

def list_files():
    """list files"""
    server = get_config()
    
    files = server.list_files()
    for data in files:
        print(data)

def list_age_files(months=1):
    """list age of files"""
    server = get_config()
    
    files = server.list_age_files(server.backup_folder)
    filtered = filter_by_age(files, months=months, filter_type=AgeFilter.ABOVE)
    for f in filtered:
        print(f)

def remove_age_files(months=1):
    """remove age of files"""
    server = get_config()
    
    files = server.list_age_files(server.backup_folder)
    filtered = filter_by_age(files, months=months, filter_type=AgeFilter.ABOVE)
    for f in filtered:
        print('remove', f)
        server.remove_file(f"{server.backup_folder}/{f}")

def clean_photos():
    """clean photos"""
    server = get_config()

    server.clean_photos()

def rename_photo():
    """rename photo"""
    server = get_config()

    server.rename_photo("nueva", ".png")

def backup_photo():
    """backup photo"""
    server = get_config()
    server.backup_file("nueva", ".png")

def main():
    """main program"""
    if len(sys.argv) == 1:
        print("No command found")
        print(COMMANDS)
        return
    cmd = sys.argv[1]

    if cmd == "list":
        list_files()
    elif cmd == "clean":
        clean_photos()
    elif cmd == "rename":
        rename_photo()
    elif cmd == "backup":
        backup_photo()
    elif cmd == "age":
        try:
            list_age_files(int(sys.argv[2]))
        except IndexError:
            list_age_files()
    elif cmd == "remove":
        try:
            remove_age_files(int(sys.argv[2]))
        except IndexError:
            remove_age_files()
    else:
        print("Unknown command")
        print(COMMANDS)

if __name__ == "__main__":
    main()