import os
import PySimpleGUI as sg
import traceback
from utils import singleton
from windows import home

if __name__ == '__main__':
    try:
        me = singleton.SingleInstance(lockfile='./abierto.lock')
        home.create_window()
    except Exception as e:
        sg.popup(traceback.format_exc(), title="Error")
    except singleton.SingleInstanceException:
        sg.popup("Aplicación ya en uso", title="App")
    os._exit(0)
