from datetime import datetime, timedelta
import json
import os
import re
from models.config import Config
from models.ftp import FTPServer
from models.item import Item
from models.nas import NAS
from models.plc import PLC
from models.server import Server
from utils import AgeFilter, filter_by_age

CONFIG_FILE = 'db/config.json'
LOG_FILE = 'db/log%s.txt'

def get_date():
    """Return the current date in iso format"""
    today = datetime.today()
    return today.date().isoformat()

def get_config():
    """Parses the configuration file

    :return: Configuration object
    """
    file = open(CONFIG_FILE, 'r')
    try:
        data = json.load(file)
        server = Server(data['server'])
        plc = PLC(data['plc'])
        nas = NAS(data['nas'])
        ftp = FTPServer(data['ftp'])
        config = Config(server, plc, nas, ftp, data['alarms'])

    finally:
        file.close()

    return config

def update_config(config: Config):
    """ Writes the config into database
    """
    try:
        data = json.dumps(config.to_dict(), indent=2)
        with open(CONFIG_FILE, 'w') as file:
            file.write(data)

    finally:
        file.close()

def write_log(text):
    """ Adds a line into daily log file
    """
    file = open(LOG_FILE % get_date(), 'a+')
    try:
        file.write(f"{text}\n")
    finally:
        file.close()

def search_by_code(code: str, filename = None):
    """ Searches the daily log file for an item given de barcode

    :param code: the barcode to search
    """
    file_path = filename if filename else LOG_FILE % get_date()
    files = get_last_month_log_files()
    for file_path in files:
        f = open(file_path, 'r')
        try:
            for line in f.readlines():
                item = Item.factory(line)
                if item.barcode == code:
                    return item
        finally:
            f.close()

def get_file_list_dict():
    """
    Returns dictionary of files

    Key is short filename

    Value is the full filename and path

    :return: Dictionary of demo files
    :rtype: Dict[str:str]
    """
    db_files_dict = {}
    for dirname, _dirnames, filenames in os.walk('db'):
        for filename in filenames:
            if filename.endswith('.txt'):
                fname_full = os.path.join(dirname, filename)
                if filename not in db_files_dict.keys():
                    db_files_dict[filename] = fname_full
                else:
                    # Allow up to 100 dupicated names. After that, give up
                    for i in range(1, 100):
                        new_filename = f'{filename}_{i}'
                        if new_filename not in db_files_dict:
                            db_files_dict[new_filename] = fname_full
                            break
    return db_files_dict

def get_last_month_log_files():
    """Returns dictionary of log files from last 30 days
    """
    file_list = get_file_list_dict()
    filtered = []
    for key, value in file_list.items():
        match = re.search(r'log(?P<year>\d+)-(?P<month>\d+)-(?P<day>\d+)\.txt', key)
        if match:
            year = match.group('year')
            month = match.group('month')
            day = match.group('day')
            file_date = datetime(int(year), int(month), int(day))
            filtered.append((value, file_date))
    return filter_by_age(filtered, months=1, filter_type=AgeFilter.BELOW)
