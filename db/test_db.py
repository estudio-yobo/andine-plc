#pylint: disable=missing-class-docstring|missing-function-docstring
import unittest
from . import search_by_code

class TestItem(unittest.TestCase):
    def test_factory(self):
        barcode = "4104978128103720197033100001010101"
        item = search_by_code(barcode, filename="db/DatosW16.txt")

        self.assertEqual(item.barcode, barcode)

if __name__ == '__main__':
    unittest.main()
