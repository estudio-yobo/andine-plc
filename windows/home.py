import PySimpleGUI as sg
from components import header, data_management, pallet_counter, image_preview, menu
import db
from utils.theme import set_theme
from windows import search, alarms, config, file_select
from models.session import Session

set_theme()

def general_layout():
    """Home main layout"""
    return [
            [sg.Column([
                [sg.Column([[
                    sg.Column([
                        [data_management.DataManagementView()],
                        [sg.Push(background_color='grey20')],
                        [pallet_counter.PalletCounterView()]
                    ], expand_x=True, background_color='grey20', p=(0,0)),
                    image_preview.ImagePreview(k='-PREVIEW_IMAGE-')
                ]], p=(20,20), expand_x=True, expand_y=True, background_color='grey20')],
            ], background_color='grey20', expand_x=True, expand_y=True, p=(0,0), scrollable=True, vertical_scroll_only=True)]
    ]

def tabs(session):
    """Home tabs layout
    """
    return [
        [
            sg.Tab('General', general_layout()),
            sg.Tab('Alarmas', alarms.alarms_layout(session)),
            sg.Tab('Buscar', search.search_layout()),
        ]
    ]

def menu_def():
    """App menubar
    """
    return [['&File', ['!&Open', '&Save::savekey', '---', '&Properties', 'E&xit']],
            ['!&Edit', ['!&Paste', ['Special', 'Normal', ], 'Undo'], ],
            ['&Debugger', ['Popout', 'Launch Debugger']],
            ['&Toolbar', ['Command &1', 'Command &2', 'Command &3', 'Command &4']],
            ['&Help', ['&About...', '&Config']], ]

def popup_yes_no():
    """Exit confirmation dialog"""
    layout = [
        [sg.Text('¿Realmente quieres salir?', size=(30, 3), justification='center')],
        [sg.Button('Si'), sg.Button('No')]
    ]
    window = sg.Window(f'Salir', layout, finalize=True, modal=True, element_justification='c')
    event, values = window.read()
    window.close()
    return event == 'Si'

def create_window():
    """Creates a standalone Home window
    """
    session = Session()
    layout = [
        header.Header("Anpar"),
        [sg.TabGroup(tabs(session), tab_location='bottomleft', tab_background_color='white', selected_background_color='grey20', selected_title_color='white', p=(0,0), expand_x=True, expand_y=True)],
        
        menu.Menu(session)
    ]
    # right_click_menu = ['', ['Version']]
    window = sg.Window("Anpar", layout=layout, resizable=True, margins=(0,0), finalize=True, enable_close_attempted_event=True)
    window.set_min_size((720,480))
    session.set_window(window)
    session.start_connections()
    while True:
        event, values = window.read()
        if event in (sg.WIN_CLOSED, sg.WINDOW_CLOSE_ATTEMPTED_EVENT): # if user closes window or clicks cancel
            if popup_yes_no():
                session.finalize()
                break
        if event == '-REPROCESS_DATA-':
            file_select.create_window(session)
        if event == '-CONFIG-':
            config.create_window(session.config)
        if event == '-SEARCH_BARCODE-':
            barcode = window['-BARCODE_INPUT-'].get()
            if len(barcode) != 34:
                search.display_error(window, 'Código no válido')
            else:
                item = db.search_by_code(barcode)
                if (item):
                    search.update_item(window, item, session.config.nas, session.config.ftp)
                else :
                    search.display_error(window, 'Elemento no encontrado')
        if event == '-CLEAN-':
            search.clear(window)
        # if event == 'Config':
        elif event == 'Version':
            sg.popup_scrolled(f'psgcompiler version 1.0.0', sg.get_versions(), f'PyInstaller version for this Python version: Nope', background_color='white')


    window.close()