import PySimpleGUI as sg
from components.alarm import make_list
from models.session import Session

def alarms_layout(session: Session):
    """Creates the alarms layout"""
    return [
        [
            sg.Column([
                [sg.Column(
                    make_list(session.config.alarms)
                , p=(20,20), expand_x=True, key='-LIST-')],
            ], background_color='grey20', expand_x=True, expand_y=True, p=(0,0))
        ]
    ]
