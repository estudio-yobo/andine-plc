import PySimpleGUI as sg
from db import get_file_list_dict
from models.item import Item

from models.session import Session
from utils import get_file_content

def get_file_list():
    """
    Returns list of filenames of files to display
    No path is shown, only the short filename
    :return: List of filenames
    :rtype: List[str]
    """
    return sorted(list(get_file_list_dict().keys()))

def show_file(window, filename):
    """Updates the UI to show file content

    :param window: The window to update the UI from
    :param filename: The path to the file the window will show 
    """
    files = get_file_list_dict()
    lines = get_file_content(files[filename])
    separator = ''
    txt = separator.join(lines)
    window[ML_KEY].update(txt)

ML_KEY = '-ML-'         # Multline's key

# --------------------------------- Create the window ---------------------------------
def make_window():
    """
    Creates the main window
    :return: The main window object
    :rtype: (sg.Window)
    """
    # First the window layout...2 columns

    left_col = [[sg.Column([
        [sg.Listbox(
            values=get_file_list(),
            select_mode=sg.SELECT_MODE_EXTENDED,
            size=(50,20),
            bind_return_key=True,
            key='-DEMO LIST-')],
    ], element_justification='l', expand_x=True, expand_y=True)]]

    right_col = [
        [sg.Multiline(size=(70, 21), write_only=True, key=ML_KEY)],
        [sg.Button('Ver'), sg.Button('Enviar al servidor')],
    ]

    # ----- Full layout -----

    layout = [
        [sg.Text('Reprocesar datos del fichero seleccionado', font='Any 20')],
        [sg.Pane([
            sg.Column(left_col, element_justification='l',  expand_x=True, expand_y=True),
            sg.Column(right_col, element_justification='c', expand_x=True, expand_y=True)
            ], orientation='h', relief=sg.RELIEF_SUNKEN, k='-PANE-')
        ],
    ]

    # --------------------------------- Create Window ---------------------------------
    window = sg.Window('Seleccionar Fichero', layout, finalize=True, resizable=True, use_default_focus=False)
    window.set_min_size(window.size)

    window['-DEMO LIST-'].expand(True, True, True)
    window[ML_KEY].expand(True, True, True)
    window['-PANE-'].expand(True, True, True)

    window.bring_to_front()
    return window


def create_window(session: Session):
    """
    The main window that contains the event loop.
    It will call the make_window function to create the window.
    """

    window = make_window()

    while True:
        event, values = window.read()

        if event in (sg.WINDOW_CLOSED, 'Exit'):
            break
        if event in ('-DEMO LIST-', 'Ver'): # if double clicked (used the bind return key parm)
            if values['-DEMO LIST-']:
                selection = values['-DEMO LIST-'][0]
                if selection:
                    show_file(window, selection)
        if event.startswith('Enviar'):
            if values['-DEMO LIST-']:
                selection = values['-DEMO LIST-'][0]
                if selection:
                    files = get_file_list_dict()
                    lines = get_file_content(files[selection])
                    if lines:
                        reads = list(map(Item.factory, lines))
                        for read in reads:
                            session.readings.append(read)

    window.close()
