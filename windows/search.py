import PySimpleGUI as sg
from components import image_preview, pallets_search, search_result
from models.ftp import FTPServer
from models.item import Item
from models.nas import NAS

def search_layout():
    """Layout showing search controls and results"""
    return [
        [
            sg.Column([
                [sg.Column([[
                    sg.Column([
                        [pallets_search.PalletsSearchView()],
                        [sg.Push(background_color='grey20')],
                        [search_result.SearchResutView()]
                    ], expand_x=True, background_color='grey20', p=(0,0)),
                    image_preview.ImagePreview(k='-SEARCH_PREVIEW_IMAGE-')
                ]], p=(20,20), expand_x=True, expand_y=True, background_color='grey20')],
            ], background_color='grey20', expand_x=True, expand_y=True, p=(0,0))
        ]
    ]

def update_item(window: sg.Window, item: Item, nas: NAS, ftp: FTPServer):
    """Updates ui based on searched item
    """
    window['-SEARCH_ERROR-'].update('', visible=False)
    window['-SEARCH_DATE-'].update(item.date.strftime('%d-%m-%Y'))
    window['-SEARCH_HOUR-'].update(item.date.strftime('%H:%M:%S'))
    window['-SEARCH_SIZE-'].update(item.size)
    window['-SEARCH_WEIGHT-'].update(item.weight)
    filename, extension = item.get_filename()
    # data = image_preview.get_img_data(f"{nas.direction}/{nas.dest_folder}/{filename}{extension}", maxsize=(330, 330))
    image_file = ftp.get_file(f"{ftp.backup_folder}/{filename}{extension}")
    if image_file:
        data = image_preview.get_img_data(image_file, maxsize=(550, 550))
        if data:
            window['-SEARCH_PREVIEW_IMAGE-'].update(data=data)

def clear(window: sg.Window):
    """Removes ui data
    """
    window['-SEARCH_ERROR-'].update('', visible=False)
    window['-SEARCH_DATE-'].update('-')
    window['-SEARCH_HOUR-'].update('-')
    window['-SEARCH_SIZE-'].update('-')
    window['-SEARCH_WEIGHT-'].update('-')
    window['-BARCODE_INPUT-'].update('')
    window['-SEARCH_PREVIEW_IMAGE-'].update(data=None)

def display_error(window: sg.Window, error: str):
    """Updates UI to show an error message
    """
    window['-SEARCH_ERROR-'].update(error, visible=True)

def create_window():
    """Creates a standalone search window
    """
    layout = search_layout()
    window = sg.Window("Búsqueda", layout=layout, resizable=True, margins=(0,0))

    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED or event == 'Cancel': # if user closes window or clicks cancel
            break
    
    window.close()