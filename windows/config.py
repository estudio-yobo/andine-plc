import os
import PySimpleGUI as sg
from models.config import Config
from models.ftp import FTPServer
from models.nas import NAS
from models.server import Server
from models.plc import PLC

server_inputs = ['host', 'port', 'country', 'country_code', 'delegation_code', 'id_class']
plc_inputs = ['plc_host']
nas_inputs = ['nas_direction', 'nas_root_folder', 'nas_dest_folder']
ftp_inputs = ['ftp_host', 'ftp_user', 'ftp_password', 'ftp_dest_folder', 'ftp_backup_folder']

def layout_generator(inputs, obj, prefix=' ', width=15):
    """Generate config layout from inputs"""
    elems = [
        [sg.Text(i.replace(prefix, '').upper().replace('_', ' '), s=width), sg.Input(default_text=obj.__getattribute__(i.replace(prefix, '')), key=i, enable_events=True)] for i in inputs
    ]
    layout = [[sg.Column(elems, p=10)]]
    return layout


def config_server_layout(server: Server):
    """Creates the form layout for the server config
    """
    return layout_generator(server_inputs, server, width=18)

def config_nas_layout(nas: NAS):
    """Creates the form layout for the nas config
    """
    return layout_generator(nas_inputs, nas, prefix='nas_', width=15)

def config_ftp_layout(ftp: FTPServer):
    """Creates the form layout for the nas config
    """
    return layout_generator(ftp_inputs, ftp, prefix='ftp_', width=25)

def config_plc_layout(plc: PLC):
    """Creates the form layout for the plc config
    """
    return layout_generator(plc_inputs, plc, prefix='plc_', width=15)

def config_layout(config: Config):
    """Creates the config tabs layout
    
    :param config: Config object with the default form values
    """
    return [
        [sg.TabGroup([
            [
                sg.Tab("Server Parcel", config_server_layout(config.server)),
                sg.Tab("PLC", config_plc_layout(config.plc)),
                sg.Tab("FTP", config_ftp_layout(config.ftp))
            ]
        ])],
        [sg.Button("Guardar")]
    ]

def create_window(config: Config):
    """
    The main window that contains the event loop.
    """
    layout = config_layout(config)

    window = sg.Window("Configuración", layout=layout, margins=(0,0), finalize=True)
    cambios = False
    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED or event == 'Cancel': # if user closes window or clicks cancel
            break
        if event in server_inputs:
            if values[event]:
                config.update_server(event, values[event])
        if event in nas_inputs:
            if values[event]:
                config.update_nas(event.replace('nas_', ''), values[event])
        if event in plc_inputs:
            if values[event]:
                config.update_plc(event.replace('plc_', ''), values[event])
        if event in ftp_inputs:
            if values[event]:
                config.update_ftp(event.replace('ftp_', ''), values[event])
        if event == 'Guardar':
            config.save()
            cambios = True
    config.restore()
    if cambios:
        event, values = sg.Window('Reiniciar',
                  [
                      [sg.T('Es necesario reiniciar para que se apliquen los cambios')],
                  [sg.T('¿Quieres cerrar la aplicación ahora?')],
                  [sg.B('SI'), sg.B('NO') ]]).read(close=True)
        if event == "SI":
            os._exit(0)
        # sg.popup('Es necesario reiniciar para que se apliquen los cambios', title='Reinicio necesario')
    window.close()