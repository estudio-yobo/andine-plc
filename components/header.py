import PySimpleGUI as sg
from .tag import Tag

def Header(title: str):
    """Header layout component
    """
    return [
        sg.Column([[
            sg.Column([[sg.Image('assets/img/andine-logo.png', size=(200,35), subsample=2)]]),
            sg.Push(),
            sg.Column([[Tag(title.upper(), "GESTION DE PALLETS", font=(None, 25))]], element_justification='center'),
            sg.Push(),
            sg.Column([[sg.Image('assets/img/logista_parcel-logo.png', size=(120,50), subsample=2)]], element_justification='right')
        ]], expand_x=True, p=(0,0))
    ]

