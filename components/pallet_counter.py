import PySimpleGUI as sg

from models.session import Session
from .tag import Tag

def PalletCounterView():
    """Pallet counter layout component
    """
    return sg.Column([
        [sg.Column([
            [Tag("CONTADOR DE PALLETS PROCESADOS", 0, key='-READS-')],

        ], p=20)]
    ], background_color='white', expand_x=True, element_justification='center', p=15)