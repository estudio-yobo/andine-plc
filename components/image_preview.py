import io
import PySimpleGUI as sg
from PIL import Image, ImageTk

def get_img_data(f, maxsize=(1100, 750), first=False):
    """Generate image data using PIL
    """
    try:
        with Image.open(f) as img:
            img.thumbnail(maxsize)
            if first:                     # tkinter is inactive the first time
                bio = io.BytesIO()
                img.save(bio, format="JPG")
                del img
                return bio.getvalue()
            return ImageTk.PhotoImage(img)
    except IOError:
        return None

def ImagePreview(k=None):
    """Image preview component"""
    return sg.Column([
        [sg.Image(k=k, expand_x=True, expand_y=True)]
    ], expand_x=True, expand_y=True, element_justification='center', vertical_alignment='center')
