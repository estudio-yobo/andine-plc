import PySimpleGUI as sg
from models.alarm import Alarm, AlarmState, has_active_alarms
from utils.theme import COLORS

ALARM_COLORS = {
    AlarmState.OK: (COLORS['SUCCESS'], COLORS['BLACK']),
    AlarmState.KO: (COLORS['WARNING'], COLORS['WHITE']),
    AlarmState.WARN: (COLORS['SECONDARY'], COLORS['BLACK']),
}

def make_list(alarms: list[Alarm]):
    """Creates a matrix of alarm titles and states
    """
    # data = [[j for j in range(2)] for i in range(len(alarms))]
    data = []
    for i, alarm in enumerate(alarms):
        data.append([
            sg.Text(alarm.title, s=45), 
            sg.Text(alarm.state._value_, k=f'-ALARM_{i}-', text_color=ALARM_COLORS.get(alarm.state)[0])
        ])
        nested_alarms = alarm.get_valid_alarms()
        for j, nested_alarm in enumerate(nested_alarms):
            data.append([
                sg.Text(nested_alarm.title, s=38, p=(30, 0, 0, 0)), 
                sg.Text(nested_alarm.state._value_ if alarm.state == AlarmState.OK else "-", k=f'-ALARM_{i}_{j}-', text_color=ALARM_COLORS.get(nested_alarm.state)[0])
            ])
    return data

def update_list(window, alarms: list[Alarm]):
    """Update alarm state"""
    if window:
        for i, alarm in enumerate(alarms):
            window[f'-ALARM_{i}-'].update(alarm.state._value_, text_color=ALARM_COLORS.get(alarm.state)[0])
            nested_alarms = alarm.get_valid_alarms()
            for j, nested_alarm in enumerate(nested_alarms):
                window[f'-ALARM_{i}_{j}-'].update(nested_alarm.state._value_ if alarm.state == AlarmState.OK else "-", text_color=ALARM_COLORS.get(nested_alarm.state)[0])
        # update alarm icon
        window['-ALARMS-'].update('assets/img/alarm.png' if has_active_alarms(alarms) else 'assets/img/bell.png')
