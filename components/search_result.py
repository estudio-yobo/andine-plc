import PySimpleGUI as sg

def SearchResutView():
    """Search Result layout component
    """
    return sg.Column([
            [sg.Column([
                [sg.Text("Fecha", size=(30, 1)), sg.Text("-", k='-SEARCH_DATE-', size=(30, 1))],
                [sg.Text("Hora", size=(30, 1)), sg.Text("-", k='-SEARCH_HOUR-', size=(30, 1))],
                [sg.Text("Medidas", size=(30, 1)), sg.Text("-", k='-SEARCH_SIZE-', size=(30, 1))],
                [sg.Text("Peso", size=(30, 1)), sg.Text("-", k='-SEARCH_WEIGHT-', size=(30, 1))],
            ], p=20)]
        ], background_color="white", expand_x=True, element_justification='center', p=15)
