import PySimpleGUI as sg
from models.session import Session

Col = sg.Sizer(100,10)

w, h = sg.Window.get_screen_size()

ELEM = 80#w / 10

def Menu(session: Session):
    """Menu layout component
    """
    return [
        [sg.Sizer(1000,1)],
        [
            sg.Column([ [sg.Image("assets/img/bell.png", key="-ALARMS-")]]),
            sg.Column([ [sg.Button("CONFIGURACIÓN", key="-CONFIG-")]]),
            sg.Column([ [sg.Button("REPROCESAR DATOS", key="-REPROCESS_DATA-")]]),
            sg.Push(),
            sg.Column([[sg.Sizer(ELEM*3,5)], [sg.Text("Servidor:"),sg.Text(session.config.server.host, key="-SERVER-", justification='center')]]),
            sg.Column([[sg.Sizer(ELEM*3,1)], [sg.Text("ANPAR v2.1.0", justification='center', expand_x=True)]]),
        ]
    ]