import PySimpleGUI as sg
from .tag import Tag

def DataManagementView():
    """Data management layout component
    """
    return sg.Column([
            [sg.Column([
                [sg.Text("GESTION DE DATOS", font=("", 20), justification='center', expand_x=True)],
                [Tag("Lectura de código de barras", "", key="-BARCODE-")],
                [sg.Column([
                    [
                        sg.Column([
                            [sg.Text("MEDIDAS (en mm.)", justification='center', expand_x=True)],
                            [sg.HSep()],
                            [Tag("LARGO", 0, key='-SIZE_WIDTH-', separator=False), Tag("ANCHO", 0, key='-SIZE_DEPTH-', separator=False), Tag("ALTO", 0, key='-SIZE_HEIGHT-', separator=False)]
                        ]),
                        sg.Push(),
                        Tag("PESO (Kgs.)", 0, key='-WEIGHT-'),
                        sg.Push(),
                        Tag('FECHA CAPTURA', "**-**-** **:**:**", key='-DATE-')
                    ]
                ], expand_x=True)]

            ], p=(60, 20), expand_x=True)]

        ], background_color="white", expand_x=True, element_justification='center', p=15)
