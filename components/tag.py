import PySimpleGUI as sg

def Tag(label, value, key=None, separator=True, **kwargs):
    """Tag layout component

    It as a text below another

    :param label: text to show in the top label
    :param value: text to show in the bottom label
    :param key: identifier to reference the value label
    """
    return sg.Column([
        [sg.Text(label, justification='center', expand_x=True, **kwargs, )],
        [sg.HSep() if separator else sg.Push()],
        [sg.Text(value, justification='center', expand_x=True, key=key, **kwargs)],
    ], expand_x=True)