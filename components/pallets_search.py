import PySimpleGUI as sg

from utils.theme import COLORS

def PalletsSearchView():
    """Pallets search layout component
    """
    return sg.Column([
            [sg.Column([
                [sg.Text("BÚSQUEDA DE PALLETS", font=("", 20), justification='center', expand_x=True)],
                [sg.Text("Lectura de código de barras", justification='center', expand_x=True)],
                [sg.Text("", justification='center', expand_x=True, text_color=COLORS['WARNING'], visible=False, k='-SEARCH_ERROR-')],
                [sg.Input(justification='center', expand_x=True, key="-BARCODE_INPUT-")],
                [sg.Button("Buscar", key='-SEARCH_BARCODE-'), sg.Button("Limpiar", key='-CLEAN-')]
            ], p=20)]
        ], background_color="white", expand_x=True, element_justification='center', p=15)
